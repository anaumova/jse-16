package ru.tsc.anaumova.tm.api.model;

import ru.tsc.anaumova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}