package ru.tsc.anaumova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showSystemInfo();

    void close();

}